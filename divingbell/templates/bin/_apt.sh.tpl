#!/bin/bash

{{/*
# Copyright 2018 AT&T Intellectual Property.  All other rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
*/}}

set -e

rm -rf /tmp/done

cat <<'EOF' > {{ .Values.conf.chroot_mnt_path | quote }}/tmp/apt.sh
{{ include "divingbell.shcommon" . }}

persist_path='/var/divingbell/apt'
declare -A CURRENT_PACKAGES
declare INSTALLED_THIS_TIME
declare TO_DELETE
declare TO_KEEP
declare REQUESTED_PACKAGES

if [ ! -d "${persist_path}" ]; then
  mkdir -p "${persist_path}"
fi

write_test "${persist_path}"

load_package_list_with_versions(){
    for f in "$@"; do
        IFS="=" read -r name version <<< $f;
        IFS=":" read -r name arch <<< $name;
        CURRENT_PACKAGES["$name"]="$version";
    done
}

wait_for_dpkg_availability(){
    count=0
    while lsof /var/lib/dpkg/lock >/dev/null 2>&1; [ $? -eq 0 ]; do
        log.WARN "dpkg is locked. retrying..."
        sleep 5
        count=$((count+1))
        if [ $count -gt 59 ]; then
            log.ERROR "dpkg is still locked after 5 minutes, exiting!"
            exit 7
        fi
    done
}

################################################
#Stage 1
#Optional pre-install command
################################################
{{- if .Values.conf.apt.pre_install }}
{{ .Values.conf.apt.pre_install }}
{{- end }}

################################################
#Stage 2
#Collect data
################################################

# First 5 lines are field descriptions
wait_for_dpkg_availability
load_package_list_with_versions $(dpkg -l | awk 'NR>5 {print $2"="$3}')
declare -A package_info; while read -r name version; do package_info["$name"]="$version N/A N/A"; done < <(dpkg -l | awk '/^ii/ {gsub(/:[^ ]*/, "", $2); print $2, $3}')

################################################
#Stage 3
#Add repositories and install/upgrade packages
################################################

{{- if hasKey .Values.conf "apt" }}
{{- if hasKey .Values.conf.apt "repositories" }}
echo -n "" > /etc/apt/trusted.gpg.d/divingbell_temp.gpg
echo "#The list of repositories managed by Divingbell" > /etc/apt/sources.list.divingbell
  {{- range .Values.conf.apt.repositories }}
    {{- $url := .url }}
    {{- $components := .components | join " " }}
    {{- $subrepos := .subrepos | default list }}
    {{- range .distributions }}
      {{- $distribution := . }}
echo "{{ printf "deb %s %s %s" $url $distribution $components }}" >>/etc/apt/sources.list.divingbell
      {{- if $subrepos }}
        {{- range $subrepos }}
echo "{{ printf "deb %s %s-%s %s" $url $distribution . $components }}" >>/etc/apt/sources.list.divingbell
        {{- end }}
      {{- end }}
    {{- end }}
    {{- if hasKey . "gpgkey" }}
apt-key --keyring /etc/apt/trusted.gpg.d/divingbell_temp.gpg add - <<"ENDKEY"
{{ .gpgkey }}
ENDKEY
    {{- end }}
  {{- end }}
mv /etc/apt/sources.list.divingbell /etc/apt/sources.list
rm -rf /etc/apt/sources.list.d/*
mv /etc/apt/trusted.gpg.d/divingbell_temp.gpg /etc/apt/trusted.gpg.d/divingbell.gpg
rm -f /etc/apt/trusted.gpg
find /etc/apt/trusted.gpg.d/ -type f ! -name 'divingbell.gpg' -exec rm {{ "{}" }} \;
rm -rf /var/lib/apt/lists/*
DEBIAN_FRONTEND=noninteractive apt-get update
{{- end }}
{{- if hasKey .Values.conf.apt "packages" }}
DEBIAN_FRONTEND=noninteractive apt-get update

{{/* Build a unified list of packages */}}
{{- $all_apt_packages := list }}
{{- if kindIs "map" .Values.conf.apt.packages }}
{{- range $k, $v := .Values.conf.apt.packages }}
{{- range $v }}
{{- $all_apt_packages = . | append $all_apt_packages }}
{{- end }}
{{- end }}
{{- else }}
{{- $all_apt_packages = .Values.conf.apt.packages }}
{{- end -}}

{{/* Filter out packages in "ignorepkgs" */}}
{{- if hasKey .Values.conf.apt "ignorepkgs" }}
{{- $new_apt_packages := list }}
{{- range $k, $v := $all_apt_packages }}
{{- if not (has $v.name $.Values.conf.apt.ignorepkgs) }}
{{- $new_apt_packages = $v | append $new_apt_packages }}
{{- end }}
{{- end }}
{{- $all_apt_packages = $new_apt_packages }}
{{- end }}

# Set all debconf selections up front
{{- range $all_apt_packages }}
{{- $pkg_name := .name }}
{{- range .debconf }}
    debconf-set-selections <<< "{{ $pkg_name }} {{ .question }} {{ .question_type }} {{ .answer }}"
{{- end }}
{{- end }}

# Run dpkg in case of interruption of previous dpkg operation
wait_for_dpkg_availability
dpkg --configure -a --force-confold,confdef

# Perform package installs
{{- if .Values.conf.apt.strict }}
{{- range $all_apt_packages }}
{{- $pkg_name := .name }}
INSTALLED_THIS_TIME="$INSTALLED_THIS_TIME {{$pkg_name}} {{- if .version }}={{ .version }}{{ end }}"
REQUESTED_PACKAGES="$REQUESTED_PACKAGES {{$pkg_name}}"
{{- end }}
{{- else }}
{{- range $all_apt_packages }}
{{- $pkg_name := .name }}
if [[ "${CURRENT_PACKAGES[{{ .name | squote }}]+isset}" != "isset"{{- if .version }} || "${CURRENT_PACKAGES[{{ .name | squote }}]}" != {{ .version }}{{- end }} ]]; then
    INSTALLED_THIS_TIME="$INSTALLED_THIS_TIME {{$pkg_name}} {{- if .version }}={{ .version }}{{ end }}"
fi
REQUESTED_PACKAGES="$REQUESTED_PACKAGES {{$pkg_name}}"
{{- end }}
{{- end }}
# Run this in case some package installation was interrupted
wait_for_dpkg_availability
DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y -o Dpkg::Options::=--force-confdef -o Dpkg::Options::=--force-confold {{- if .Values.conf.apt.allow_downgrade }} "--allow-downgrades" {{ end }}{{- if .repo }} -t {{ .repo }}{{ end }} $INSTALLED_THIS_TIME
{{- end }}

# Perform package upgrades
{{- if .Values.conf.apt.upgrade }}
wait_for_dpkg_availability
DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true apt-get dist-upgrade \
    -y -o Dpkg::Options::=--force-confdef -o Dpkg::Options::=--force-confold

if [[ -f "/var/run/reboot-required" ]]; then
  log.INFO 'System reboot REQUIRED.'
fi
{{- end }}
{{- end }}

################################################
#Stage 4
#Remove packages not present in conf.apt anymore
################################################

{{- if .Values.conf.apt.strict }}
# For strict mode, we do not want to use --autoremove, to avoid
# letting apt remove packages outside divingbell's control
APT_PURGE="apt-get purge -y --allow-remove-essential"
{{- else }}
APT_PURGE="apt-get purge -y --autoremove"
{{- end }}

{{- if .Values.manifests.daemonset_apt_remove_old_pkgs }}
{{- if hasKey .Values.conf.apt "packages" }}
{{- if .Values.conf.apt.strict }}
# in strict mode we execute this stage even on first run, so
# touch the packages file here to avoid the short-circuit below
touch ${persist_path}/packages
{{- end }}

{{ $whitelist_pkgs := list }}
{{- if hasKey .Values.conf.apt "whitelistpkgs" }}
{{- range .Values.conf.apt.whitelistpkgs }}
{{- $whitelist_pkgs = . | append $whitelist_pkgs }}
{{- end }}
{{- end }}
{{- if hasKey .Values.conf.apt "ignorepkgs" }}
{{- range .Values.conf.apt.ignorepkgs }}
{{- $whitelist_pkgs = . | append $whitelist_pkgs }}
{{- end }}
{{- end }}

echo $INSTALLED_THIS_TIME | sed 's/ /\n/g' | sed '/^[[:space:]]*$/d' | sort > ${persist_path}/packages.new
echo $REQUESTED_PACKAGES | sed 's/ /\n/g' | sed '/^[[:space:]]*$/d' | sort > ${persist_path}/packages.requested
if [ -f ${persist_path}/packages ]; then
    # if strict mode, we reload the current package list to ensure we have an accurate list to audit from
    # (e.g., in case a package was requested but not installed for some reason)
    # note that in strict mode, $CURRENT_PACKAGES will duplicate the packages in $INSTALLED_THIS_TIME but in
    # non-strict mode (which has logic to use the "packages" file it writes so it doesn't touch anything it
    # didn't originally install) it doesn't.
    {{- if .Values.conf.apt.strict }}
    wait_for_dpkg_availability
    load_package_list_with_versions $(dpkg -l | awk 'NR>5 {print $2"="$3}')
    {{- end }}
    for package in "${!CURRENT_PACKAGES[@]}"
    do
        CURRENT_PACKAGE_NAMES="$CURRENT_PACKAGE_NAMES $package"
    done
    echo $CURRENT_PACKAGE_NAMES | sed 's/ /\n/g' | sed '/^[[:space:]]*$/d' | sort > ${persist_path}/packages.current
    {{- if .Values.conf.apt.strict }}
    TO_DELETE=$(comm -23 ${persist_path}/packages.current ${persist_path}/packages.requested)
    TO_KEEP=$(echo "$TO_DELETE" | comm -23 ${persist_path}/packages.current -)
    {{- else }}
    TO_DELETE=$(comm -23 ${persist_path}/packages ${persist_path}/packages.requested)
    TO_KEEP=$(echo "$TO_DELETE" | comm -23 ${persist_path}/packages -)
    {{- end }}
    if [ ! -z "$TO_DELETE" ]; then
        wait_for_dpkg_availability
        dpkg --configure -a --force-confold,confdef

        {{- if $whitelist_pkgs }}
        WHITELIST=({{ include "helm-toolkit.utils.joinListWithSpace" $whitelist_pkgs }})
        {{- end }}
        PURGE_LIST=""
        while read -r pkg; do
            {{- if $whitelist_pkgs }}
            found=false
            for item in "${WHITELIST[@]}"; do
                if [[ "${item}" == "${pkg}" ]]; then
                    found=true
                    break
                fi
            done
            if [[ "${found}" == "false" ]]; then
                PURGE_LIST="$PURGE_LIST $pkg"
            fi
            {{- else }}
            PURGE_LIST="$PURGE_LIST $pkg"
            {{- end }}
        done <<< "$TO_DELETE"
        DEBIAN_FRONTEND=noninteractive $APT_PURGE $PURGE_LIST
    fi
    if [ ! -z "$TO_KEEP" ]; then
        echo "$TO_KEEP" > ${persist_path}/packages
    else
        rm ${persist_path}/packages
    fi
fi
if [ ! -z "$INSTALLED_THIS_TIME" ]; then
{{- if not .Values.conf.apt.strict }}
    cat ${persist_path}/packages.new >> ${persist_path}/packages
{{- end }}
    sort ${persist_path}/packages -o ${persist_path}/packages
fi
{{- end }}
{{- end }}

######################################################
#Stage 5
#Remove blacklisted packages in conf.apt.blacklistpkgs
######################################################

{{- if hasKey .Values.conf.apt "blacklistpkgs" }}
wait_for_dpkg_availability
dpkg --configure -a --force-confold,confdef
{{- range .Values.conf.apt.blacklistpkgs }}
  {{- $package := . }}
  DEBIAN_FRONTEND=noninteractive $APT_PURGE {{ $package | squote }}
{{- end }}
{{- end }}

######################################################
#Stage 6
#Verify that all package versions are correct and latest
######################################################
{{- if .Values.conf.apt.upgrade }}
while read -r name version; do
  if [[ "${package_info[$name]+exists}" ]]; then
    package_info["$name"]="$(echo ${package_info[$name]} | awk -v val=$version '{$2 = val; print}')"
  else
    package_info["$name"]="N/A $version N/A"
  fi
done < <(dpkg -l | awk '/^ii/ {gsub(/:[^ ]*/, "", $2); print $2, $3}')

while read -r name version; do
  if [ -n "$version" ]; then
    package_info["$name"]="$(echo ${package_info[$name]} | awk -v val=$version '{$3 = val; print}')"
  fi
done < <(apt-cache madison "${!package_info[@]}" | awk '!a[$1]++ {print $1 " " $3}')

{{- if hasKey .Values.conf.apt "whitelistpkgs" }}
whitelist=({{ join " " .Values.conf.apt.whitelistpkgs }})
{{- else }}
whitelist=()
{{- end }}

for pkg in "${!package_info[@]}"; do
  read -r before after available < <(echo "${package_info[$pkg]}")
  if [[ "$after" != "N/A" && "$after" != "$available" && ! ${whitelist[*]} =~ "$pkg" ]]; then
    log.ERROR "Package $pkg was not properly installed/upgraded, installed version $after, available version $available"
    exit 1
  fi
  if [[ "$before" != "$after" ]]; then
    [[ "$before" == "N/A" && "$after" != "N/A" ]] && log.INFO "$pkg installed ver $after"
    [[ "$before" != "N/A" && "$after" == "N/A" ]] && log.INFO "$pkg removed ver $before"
    [[ "$before" != "N/A" && "$after" != "N/A" ]] && log.INFO "$pkg upgraded ver $before => $after"
  fi
done
{{- end }}

######################################################
#Stage 7
#Run optional post-install command
######################################################
{{- if .Values.conf.apt.post_install }}
{{ .Values.conf.apt.post_install }}
{{- end }}
log.INFO 'Putting the daemon to sleep.'
EOF

chmod 755 {{ .Values.conf.chroot_mnt_path | quote }}/tmp/apt.sh
chroot {{ .Values.conf.chroot_mnt_path | quote }} /tmp/apt.sh

touch /tmp/done

while [ 1 ]; do
  sleep 300
done

exit 0
